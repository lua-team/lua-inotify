Source: lua-inotify
Section: interpreters
Priority: optional
Maintainer: Sophie Brun <sophie@freexian.com>
Uploaders: Raphaël Hertzog <hertzog@debian.org>, Mathieu Parent <sathieu@debian.org>
Build-Depends: debhelper-compat (= 13), dh-lua (>= 24), pkg-config
Standards-Version: 4.6.0
Homepage: https://github.com/hoelzro/linotify
Vcs-Git: https://salsa.debian.org/lua-team/lua-inotify.git
Vcs-Browser: https://salsa.debian.org/lua-team/lua-inotify

Package: lua-inotify
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: inotify bindings for Lua
 This package contains Lua bindings for the Linux inotify API.
 .
 The inotify API lets you monitor filesystem events (access, update, creation,
 deletion, rename, etc.) on watched paths (both files and directories).

Package: lua-inotify-dev
Section: libdevel
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Architecture: any
Depends: lua-inotify (= ${binary:Version}), ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: inotify bindings for Lua (development files)
 This package contains the development files of the lua-inotify library,
 useful to create a statically linked binary (like a C application or a
 standalone Lua interpreter).
 .
 Documentation is also shipped within this package.
